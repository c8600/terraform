module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = local.full_cluster_name
  cluster_version = var.kubernetes_version
  subnet_ids      = split(",",trim(var.private_subnet_ids, "[]"))
  version         = "~> 18.20"

  cluster_endpoint_public_access = false
  cluster_endpoint_private_access = true
 

  tags = local.common_tags

  vpc_id = var.vpc_id

  cluster_security_group_additional_rules = {
    ingress_https_vpc = {
      description      = "Allow HTTPS from VPC CIDR"
      protocol         = "tcp"
      from_port        = 443
      to_port          = 443
      type             = "ingress"
      cidr_blocks      = [var.vpc_cidr]
    }

    ingress_https_gitlab = {
      description      = "Allow HTTPS from Gitlab"
      protocol         = "tcp"
      from_port        = 443
      to_port          = 443
      type             = "ingress"
      cidr_blocks      = ["172.31.0.0/16"]
    }
  }
  eks_managed_node_group_defaults = {
    instance_types = ["m5.large"]

    # We are using the IRSA created in iam.tf for permissions
    # However, we have to deploy with the policy attached FIRST (when creating a fresh cluster)
    # and then turn this off after the cluster/node group is created. Without this initial policy,
    # the VPC CNI fails to assign IPs and nodes cannot join the cluster
    # See https://github.com/aws/containers-roadmap/issues/1666 for more context
    iam_role_attach_cni_policy = true
  }

  eks_managed_node_groups = {
    backend-pool = {
      # By default, the module creates a launch template to ensure tags are propagated to instances, etc.,
      # so we need to disable it to use the default template provided by the AWS EKS managed node group service
      create_launch_template = false
      launch_template_name   = ""
      instance_types   = var.backend_pool.instance_type
      desired_size = var.backend_pool.desired_capacity
      max_size     = var.backend_pool.max_size
      min_size     = var.backend_pool.min_size
      labels = {
        Environment = "dev"
        GithubRepo  = "terraform-aws-eks"
        GithubOrg   = "terraform-aws-modules"
        backend     = "true"
        pool        = "backend"
      }
    }
    logs-pool = {
      # By default, the module creates a launch template to ensure tags are propagated to instances, etc.,
      # so we need to disable it to use the default template provided by the AWS EKS managed node group service
      create_launch_template = false
      launch_template_name   = ""
      instance_types   = var.logs_pool.instance_type
      desired_size = var.logs_pool.desired_capacity
      max_size     = var.logs_pool.max_size
      min_size     = var.logs_pool.min_size
      labels = {
        Environment = "dev"
        GithubRepo  = "terraform-aws-eks"
        GithubOrg   = "terraform-aws-modules"
        logs        = "true"
        pool        = "logs"
      }
    }
  }

  manage_aws_auth_configmap = true

  aws_auth_users = var.map_users
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

