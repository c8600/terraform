provider "aws" {
  region = var.region
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}
terraform {
  required_version = "~> 1.1"
  required_providers {
    aws = {
      version = "~> 3.75"
      source  = "hashicorp/aws"
    }
    kubernetes = {
      version = ">= 2.10.0"
    }
    helm = {
      version = ">= 2.5.1"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.14.0"
    }
  }
  backend "s3" {
    region = "eu-north-1"
    key = "dev/eks.tfstate"
    bucket = "ecosia-infra"
  }

}
