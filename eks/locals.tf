locals {
  full_cluster_name = "${var.env}-${var.cluster_name}"

  common_tags = {
    Owner = var.owner
    Team  = "ecosia"
    Env   = var.env

    Terraform = true
  }

  cluster_autoscale_service_account_namespace = "kube-system"
  cluster_autoscale_service_account_name      = "cluster-autoscaler"
}
