module "wireguard" {
  source = "../../wireguard"
  vpc_id =var.vpc_id
  subnet_id = var.public_subnet_ids
  owner = var.owner
}
  output "wg_config" {
    value = module.wireguard.wg_config
}
