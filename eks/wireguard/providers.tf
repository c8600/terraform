provider "aws" {
  region = var.region
}

terraform {
  required_version = "~> 1.1.0"
  required_providers {
    aws = {
      version = "~> 3.75"
      source  = "hashicorp/aws"
    }
  }
}
