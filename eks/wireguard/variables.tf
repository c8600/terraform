variable "public_subnet_ids" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "owner" {
  type = string
}