module "vpc_peering" {
  source = "cloudposse/vpc-peering/aws"
  version = "0.9.2"
  namespace        = "ecosia"
  stage            = "dev"
  name             = "peering"
  requestor_vpc_id = var.peering_vpc_id
  acceptor_vpc_id  = var.vpc_id
  tags = local.common_tags
  auto_accept = true
}