resource "aws_security_group" "from_peered_vpc_to_nodeport" {
  name_prefix = "from_peered_vpc"
  vpc_id = var.vpc_id

  tags = local.common_tags

  ingress {
    from_port = 30000
    to_port   = 32767
    protocol  = "tcp"

    cidr_blocks = [
      "172.31.0.0/16",
    ]
  }

  ingress {
    from_port = 30000
    to_port   = 32767
    protocol  = "udp"

    cidr_blocks = [
      "172.31.0.0/16",
    ]
  }
}
