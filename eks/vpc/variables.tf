

variable "region" {
  default = "eu-north-1"
}

variable "cluster_name" {
  default = "ecosia"
}

variable "owner" {
  type = string
}

variable "env" {
  type = string
}

variable "vpc_cidr" {
  type = string
  default =  "10.200.0.0/17"
}

