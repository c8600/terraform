
output "private_subnets" {
    value = module.vpc.private_subnets
}
output "public_subnet_ids" {
    value = module.vpc.public_subnets[0]
}
output "vpc_id" {
    value = module.vpc.vpc_id
}
output "test" {
     value = tolist(module.vpc.public_route_table_ids)
}

output "vpc_cidr" {
    value = module.vpc.vpc_cidr_block
}