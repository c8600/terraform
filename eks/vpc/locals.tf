
locals {
  full_cluster_name = "${var.env}-${var.cluster_name}"
    common_tags = {
    Owner = var.owner
    Team  = "ecosia"
    Env   = var.env

    Terraform = true
  }
}
