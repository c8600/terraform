provider "aws" {
  region = var.region
}

terraform {
  required_version = "~> 1.1"
  required_providers {
    aws = {
      version = "~> 3.75"
      source  = "hashicorp/aws"
    }
  }
  backend "s3" {
    region = "eu-north-1"
    key = "dev/vpc.tfstate"
    bucket = "ecosia-infra"
  }
}
