data "aws_availability_zones" "available" {}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.14"

  name                 = "${local.full_cluster_name}-vpc"
  cidr                 = var.vpc_cidr
  azs                  = slice(data.aws_availability_zones.available.names, 0, 3) # TODO variables
  private_subnets      = [for i in [0, 1, 2] : cidrsubnet(var.vpc_cidr, 3, i)]
  public_subnets       = [for i in [3, 4, 5] : cidrsubnet(var.vpc_cidr, 3, i)]
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
  manage_default_route_table = false

  tags = local.common_tags

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.full_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"                  = 1
  }
#  public_subnet_tags = {
#     "kubernetes.io/cluster/${local.full_cluster_name}" = "shared"
#     "kubernetes.io/role/elb"                  = 1
#   }
}
