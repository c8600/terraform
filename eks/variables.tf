

variable "region" {
  default = "eu-north-1"
}

variable "backend_pool" {
  default = {
    instance_type    = ["m5.large"]
    desired_capacity = 1
    max_size         = 2
    min_size         = 1
  }
}

variable "logs_pool" {
  default = {
    instance_type    = ["m5.large"]
    desired_capacity = 1
    max_size         = 2
    min_size         = 1
  }
}
variable "owner" {
  type = string
}

variable "env" {
  type = string
  default = "dev"
}

variable "kubernetes_version" {
  // https://docs.aws.amazon.com/eks/latest/userguide/kubernetes-versions.html
  // Use only `major.minor`, omit the `patch` part

  default = "1.22"
}


variable "cluster_name" {
  default = "ecosia"
}


variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::<ID>:user/aokhotnikov"
      username = "aokhotnikov"
      groups   = ["system:masters"]
    },

    {
      userarn  = "arn:aws:iam::<ID>:user/deploy"
      username = "deploy"
      groups   = ["system:masters"]
    },

  ]
}

variable "private_subnet_ids" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "vpc_cidr" {
  type = string
}
variable "peering_vpc_id" {
  type = string
}