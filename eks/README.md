# Prerequisites

- wireguard
    https://www.wireguard.com/install/
- ansbile
    https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
- aws-cli
    https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html
- terraform
    https://learn.hashicorp.com/tutorials/terraform/install-cli#install-terraform
- kubectl
    https://kubernetes.io/docs/tasks/tools/
- helm
    https://helm.sh/docs/intro/install/

# Config

Most config options are set in the config.auto.tfvar file which are input variables. More info TODO

# Usage

This module uses Makefile as a driver. This is due to EKS private endpoint enabled cluster requires conectivity to deploy the module and here it is handled by first creating the VPC and a Wireguard instance after which WG file is used to create the connection and EKS is deployed last.

steps:
``` make init-all ```
then create the VPC and the WireGuard instance
``` make deploy-vpc```
after which you are able to connect to WireGuard. Keep in mind that it will ask you for sudo credentials due to needing to create virtual interfaces. 
``` make deploy-wg```
which will create your own instance of WG to allow connection. Needed foor interacting with eks
``` make connect-vpn ```
then you are able to install the EKS cluster
``` make deploy-eks```
which will allow you to grab the kubeconfig file by issuing
``` init-kubectl```
