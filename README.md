# terraform

The process is as follows:

- Provision VPC intended to hold al objects and EKS in particular
- Provision jump host as EKS API endpoint is not going to be exposed outside VPC (or use vpc peering)
- Provision EKS including IRSA roles to provide options to control DNS/Storage/LB capabilities from within EKS

Refer to [makefile](eks/makefile)

Reconsiliation loop can be performed by CICD pipelines