terraform {
  required_providers {
    aws = {
      version = "~> 3.50"
    }
    local = {
      version = "~> 2.1"
    }
    null = {
      version = "~> 3.1"
    }
    tls = {
      version = "~> 3.1"
    }
    template = {
      version = "~> 2.2"
    }
  }
}


provider "aws" {
  region = "eu-north-1"
}
