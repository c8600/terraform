output "ec2-public" {
  value = aws_instance.vpn.public_ip
}

output "ssh_command" {
  value = [
    "ssh -l ubuntu -i ${local_file.pem_file.filename} ${aws_instance.vpn.public_ip}"
  ]
  }

output "wg_config" {
  value = abspath("${path.module}/ansible/client.conf")
}