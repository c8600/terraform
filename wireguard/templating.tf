resource "local_file" "cloud_init" {
    content     = templatefile("${path.module}/templates/cloud-init.tmpl",{})
    filename = "${path.module}/renders/cloud_init"
}

resource "local_file" "hosts_cfg" {
  content = templatefile("${path.module}/templates/hosts.cfg.tmpl",
    {
      wireguard = aws_instance.vpn.public_ip
      key       = local_file.pem_file.filename
    }
  )
  filename = "${path.module}/ansible/hosts.cfg"
}