

resource "tls_private_key" "user_key" {
  algorithm   = "RSA"
  rsa_bits  = 4096
}
resource "aws_key_pair" "user_key_pair" {
  key_name_prefix   = "${var.owner}-wg-"
  public_key = tls_private_key.user_key.public_key_openssh
}


resource "local_file" "pem_file" {
  filename = pathexpand("./${aws_key_pair.user_key_pair.id}.key")
  file_permission = "600"
  directory_permission = "700"
  sensitive_content = tls_private_key.user_key.private_key_pem
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "vpn" {
  user_data = local_file.cloud_init.content
  ami             = data.aws_ami.ubuntu.id
  instance_type   = "t3.micro"
  key_name        = aws_key_pair.user_key_pair.id
  vpc_security_group_ids = [aws_security_group.wireguard-sg.id]
  subnet_id = var.subnet_id
  associate_public_ip_address = true
  tags = {
    Name = "${var.owner}-Wireguard"
    Owner = "${var.owner}"
  }
}



resource "aws_security_group" "wireguard-sg" {
  name        = "${var.owner}-wireguard-sg"
  description = "SG for wireguard access"
  vpc_id = var.vpc_id
  
  
  ingress {
    from_port = 8
    protocol = "icmp"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "WIREVPN PORT"
    from_port   = 51820
    to_port     = 51820
    protocol    = "UDP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${var.owner}-Wireguard-SG"
    Owner = "${var.owner}"
  }
}
resource "null_resource" "ansible" {
  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ${path.module}/ansible/hosts.cfg ${path.module}/ansible/playbook.yml"
  }
  depends_on = [
    local_file.hosts_cfg,
    aws_instance.vpn,
  ]
}


